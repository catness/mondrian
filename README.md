# About the project

**Mondrian** is an example for [Android Kotlin Fundamentals: Constraint Layout](https://codelabs.developers.google.com/codelabs/kotlin-android-training-constraint-layout/index.html#0), which reproduces Mondrian's painting [Abstract Cubes Squares](https://www.amazon.co.uk/Wee-Blue-Coo-MONDRIAN-GREETINGS/dp/B00TSINNG8) (not sure if it's its real name, I couldn't find any official reference to it - only to art prints and other consumer products) as a collection of TextViews organized in one ConstraintLayout. Every individual TextView randomly changes color on clicking, and resets to white on long-clicking.

The xml layout was generated with a Python script (see util directory) from a .toml file with a user-friendly description of how the boxes are connected together. (I find Android Studio's Layout Editor too complicated to use, at least for such a complex layout.)

Text views are surrounded with borders, using the rectangle.xml resource as a background.

The app only works in portrait mode. Box sizes are calculated in proportion to width, so when width becomes bigger than height, the picture just doesn't fit. Also, when switching to landscape and back, the white boxes sometimes change color to some random color. I'm leaving it as a feature :)

The apk is here: [mondrian.apk](apk/mondrian.apk).

---

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
