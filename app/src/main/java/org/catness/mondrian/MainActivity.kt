package org.catness.mondrian

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.widget.TextView
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var boxes : List<TextView> = listOf(box_1,box_2,box_3,box_4,box_5,box_6,box_7,box_8,box_9,box_10,
            box_11,box_12,box_13,box_14,box_15,box_16,box_17,box_18,box_19,box_20,
            box_21,box_22,box_23,box_24,box_25,box_26,box_27,box_28,box_29,box_30,
            box_31,box_32,box_33,box_34,box_35,box_36,box_37,box_38,box_39,box_40,
            box_41,box_42,box_43
            )

        // some of the colors on Mondrian's painting differ from standard Android colors
        // so we define them in colors.xml
        listOf(box_2,box_9,box_20,box_21,box_22,box_23,box_35,box_37,box_42).forEach( { setColor(it,ContextCompat.getColor(this, R.color.yellow)) } ) 
        listOf(box_8,box_15,box_27).forEach( { setColor(it,ContextCompat.getColor(this, R.color.red)) } )
        listOf(box_16,box_28,box_30).forEach( { setColor(it, ContextCompat.getColor(this, R.color.blue)) } )
        listOf(box_7,box_11,box_33,box_34,box_38).forEach( { setColor(it, Color.BLACK) } )

        boxes.forEach( { it.setOnClickListener { changeColor(it) } } )
        // long click listener must return a boolean, to tell the system if the click was consumed
        boxes.forEach( { it.setOnLongClickListener { 
            setColor(it,Color.WHITE) 
            true
        } } )
    }

    private fun changeColor(view: View) {
        // https://stackoverflow.com/questions/54021762/how-do-i-set-the-border-colour-of-my-textview-programmatically
        var layerDrawable : LayerDrawable = view.getBackground() as LayerDrawable
        var gradientDrawable : GradientDrawable = layerDrawable.findDrawableByLayerId(R.id.box) as GradientDrawable

        // Change background color
        var color : Int = Color.argb(255,(0..255).random(), (0..255).random(), (0..255).random())   
        gradientDrawable.setColor(color)
        // Change stroke color. (Assumes 4px stroke width.)
        //gradientDrawable.setStroke(4, Color.parseColor("#FF0000"))
    }

    private fun setColor(view: View, color: Int)  {
        var layerDrawable : LayerDrawable = view.getBackground() as LayerDrawable
        var gradientDrawable : GradientDrawable = layerDrawable.findDrawableByLayerId(R.id.box) as GradientDrawable
        gradientDrawable.setColor(color)
    }


}