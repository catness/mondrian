#!/usr/bin/python3
"""
Converts a .toml formatted file with the description of the layout
into Android Studio activity_main.xml ConstraintLayout.
"""

import re, tomlkit

debug = False

layout_t = """<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


   <androidx.constraintlayout.widget.Guideline
        android:id="@+id/guideline"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        app:layout_constraintGuide_percent="0.5"/>

{views}

</androidx.constraintlayout.widget.ConstraintLayout>
"""

box_t = """
  <TextView
        android:id="@+id/box_{num}"
        android:text="{text}"
        style="@style/BoxStyle"
        android:layout_width="0dp"
        android:layout_height="0dp"
        app:layout_constraintWidth_default="percent"
        app:layout_constraintWidth_percent="{width}" 
        {height}
{edges} />

"""

height_pt = """
        app:layout_constraintHeight_default="percent"
        app:layout_constraintHeight_percent="{height}" 
"""
height_rt = """
        app:layout_constraintDimensionRatio="{height}" 
"""

edge_t = """        app:layout_constraint{src}_to{dst}Of="@+id/box_{num}" 
"""

edge_pt = """        app:layout_constraint{src}_to{src}Of="parent" 
"""
edge_g = """        app:layout_constraint{src}_to{dst}Of="@+id/guideline" 
"""


inverse = {"top":"bottom","bottom":"top","left":"right","right":"left"}


def make_box(box):
    height = box['height']
    if height == "spread":
        height_str = "app:layout_constraintHeight_default=\"spread\"\n"
    elif ':' in str(height):
        height_str = height_rt.format(height=height)
    else:
        height_str = height_pt.format(height=height)

    edges = ""
    for edge in ["left","right","top","bottom"]:
        if edge in box:
            if box[edge] == "parent":
                edges += edge_pt.format(src=edge.capitalize())
            elif box[edge] == "guideline":
                edges += edge_g.format(src=edge.capitalize(),dst=inverse[edge].capitalize())
            else:
                dstnum,dst = box[edge].split(':')
                edges += edge_t.format(src=edge.capitalize(),dst=dst.capitalize(),num=dstnum)

    text = str(box['num']) if debug else ''
    out = box_t.format(num=box['num'],text=text,width=box['width'],height=height_str,edges=edges)
    return out

filename = "mondrian.toml"
with open(filename,"r") as f:
    document = f.read()

y = tomlkit.parse(document)
views = ""
for box in y['box']:
    views += make_box(box)
layout = layout_t.format(views=views)

with open("activity_main.xml","w") as f:
    f.write(layout)

